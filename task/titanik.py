import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age'
     and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['Title'] = df['Name'].apply(lambda name: name.split(",")[1].split(".")[0].strip())
    #med_Age = df['Age'].median()
    #df[Age].fillna(med_Age, inplace = True)
    t = ["Mr","Mrs","Miss"]

    r = []

    for ts in t:
        m_age = df.loc[dfp['Title']] == ts, 'Age'.median()
        missing_val = df.loc[dfp['Title']] == ts, 'Age'.isna().sum()
        r.append(t+'.',missing_val,round(m_age))
    return r
